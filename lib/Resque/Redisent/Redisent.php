<?php
namespace Resque\Redisent;

if( ! defined('CRLF')) define('CRLF', sprintf('%s%s', chr(13), chr(10)));

/**
 * Redisent, a Redis interface for the modest
 * @author Justin Poliey <jdp34@njit.edu>
 * @copyright 2009 Justin Poliey <jdp34@njit.edu>
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @package Redisent
 */
class Redisent {
    /**
     * Max allowed zero fwrite return before connection retry
     * @var int
     */
    public const MAX_ALLOWED_ZERO_SIZE_COUNT = 100;

    /**
     * Socket connection to the Redis server
     * @var resource
     * @access private
     */
    private $__sock;

    /**
     * Host of the Redis server
     * @var string
     * @access public
     */
    public $host;

    /**
     * Port on which the Redis server is running
     * @var integer
     * @access public
     */
    public $port;

    /**
     * Creates a Redisent connection to the Redis server on host {@link $host} and port {@link $port}.
     * Redisent constructor.
     * @param $host
     * @param int $port
     * @throws \Exception
     */
    public function __construct($host, $port = 6379) {
        $this->host = $host;
        $this->port = $port;
        $this->establishConnection();
    }

    /**
     * Create a connection
     * @throws \Exception
     */
    public function establishConnection() {
        $this->__sock = fsockopen($this->host, $this->port, $errno, $errstr);
        if (!$this->__sock) {
            throw new \Exception("{$errno} - {$errstr}");
        }
    }

    /**
     * Close connection
     */
    public function __destruct() {
        fclose($this->__sock);
    }

    /**
     * Execute a Redis command
     * @param $name
     * @param $args
     * @return array|bool|int|null|string
     * @throws RedisException
     */
    public function __call($name, $args) {

        /* Build the Redis unified protocol command */
        array_unshift($args, strtoupper($name));
        $command = sprintf('*%d%s%s%s', is_countable($args) ? count($args) : 0, CRLF, implode(CRLF, array_map([$this, 'formatArgument'], $args)), CRLF);

        /* Open a Redis connection and execute the command */
        $zeroSizeCount = 0;
        for ($written = 0; $written < strlen($command); $written += $fwrite) {
            $fwrite = fwrite($this->__sock, substr($command, $written));
            if ($fwrite === FALSE) {
                throw new \Exception('Failed to write entire command to stream');
            }

            // Avoid infinite loop on connection lost and retry connection
            if ($fwrite === 0) {
                $zeroSizeCount++;
                if ($zeroSizeCount == self::MAX_ALLOWED_ZERO_SIZE_COUNT) {
                    $this->establishConnection();
                }
            } else {
                $zeroSizeCount = 0;
            }
        }

        /* Parse the response based on the reply identifier */
        $reply = trim(fgets($this->__sock, 512));
        switch (substr($reply, 0, 1)) {
            /* Error reply */
            case '-':
                throw new RedisException(substr(trim($reply), 4));
            /* Inline reply */
            case '+':
                $response = substr(trim($reply), 1);
                break;
            /* Bulk reply */
            case '$':
                $response = null;
                if ($reply == '$-1') {
                    break;
                }
                $read = 0;
                $size = substr($reply, 1);
                do {
                    $block_size = ($size - $read) > 1024 ? 1024 : ($size - $read);
                    $response .= fread($this->__sock, $block_size);
                    $read += $block_size;
                } while ($read < $size);
                fread($this->__sock, 2); /* discard crlf */
                break;
            /* Multi-bulk reply */
            case '*':
                $count = substr($reply, 1);
                if ($count == '-1') {
                    return null;
                }
                $response = [];
                for ($i = 0; $i < $count; $i++) {
                    $bulk_head = trim(fgets($this->__sock, 512));
                    $size = substr($bulk_head, 1);
                    if ($size == '-1') {
                        $response[] = null;
                    }
                    else {
                        $read = 0;
                        $block = "";
                        do {
                            $block_size = ($size - $read) > 1024 ? 1024 : ($size - $read);
                            $block .= fread($this->__sock, $block_size);
                            $read += $block_size;
                        } while ($read < $size);
                        fread($this->__sock, 2); /* discard crlf */
                        $response[] = $block;
                    }
                }
                break;
            /* Integer reply */
            case ':':
                $response = intval(substr(trim($reply), 1));
                break;
            default:
                throw new RedisException("invalid server response: {$reply}");
        }
        /* Party on */
        return $response;
    }

    /**
     * Format argument
     * @param $arg
     * @return string
     */
    private function formatArgument($arg) {
        return sprintf('$%d%s%s', strlen($arg), CRLF, $arg);
    }
}